import React from 'react';
import {StyleSheet, Button, View } from 'react-native';

export default function Result({ resultCounter }) {
    return (
        <View style={styles.container}>
            <Button title="Посчитать" onPress={resultCounter} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: '50%',
      marginBottom: 20,
    },
});