import React from 'react';
import { Slider, View, Text, StyleSheet } from 'react-native'

export default function SliderInput({ sliderValue, setSliderValue }) {

    return (
        <View style={styles.container}>
            <Slider
                    style={{width: 200, height: 40}}
                    minimumValue={0}
                    maximumValue={10}
                    step={1}
                    value={sliderValue}
                    onValueChange={(value) => setSliderValue(value)}
             />
             <Text>Число: {sliderValue}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '50%',
        alignItems: 'center',
        marginBottom: 20,
    },
});
