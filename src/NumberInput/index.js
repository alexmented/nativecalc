import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

export default function NumberInput({ number, setNumber }) {

    const onChange = (text) => setNumber(text);
    return (
        <View style={styles.container}>
            <TextInput value={number} keyboardType="number-pad" autoCorrect={false} onChangeText={onChange} placeholder="Число" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '50%',
        padding: 5,
        marginTop: 15,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'black'
    },
});
