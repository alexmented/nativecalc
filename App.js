import React, { useState } from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import NumberInput from './src/NumberInput'
import Result from './src/Result'
import SliderInput from './src/SliderInput'

export default function App() {
  const [number, setNumber] = useState('');
  const [sliderValue, setSliderValue] = useState(0);
  const [result, setResult] = useState(0);

  const resultCounter = () => {
      if ((Number(number)) || (number === '0')) {
          setResult(parseInt(number) * sliderValue);
          setNumber('');
          setSliderValue(0);
      }
      else {
          Alert.alert(Number(number));
          Alert.alert('Ошибка, введи еще раз')
      }
  };

  return (
    <View style={styles.container}>
      <NumberInput number={number} setNumber={setNumber}/>
      <SliderInput sliderValue={sliderValue} setSliderValue={setSliderValue} />
      <Text>Результат: {result}</Text>
      <Result resultCounter={resultCounter}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 25,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});
